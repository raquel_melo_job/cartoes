package com.itau.cartoes.services;

import com.itau.cartoes.models.Cartao;
import com.itau.cartoes.models.Pagamento;
import com.itau.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoService cartaoService;

    public Optional<Pagamento> buscarPorId(int id){
        Optional<Pagamento> optionalPagamento = pagamentoRepository.findById(id);
        return optionalPagamento;
    }

    public Pagamento salvarPagamento(Pagamento pagamento) {
        Pagamento pagamentoObjeto = pagamentoRepository.save(pagamento);
        return pagamentoObjeto;
    }

    public boolean cartaoExiste(Pagamento pagamento){
        Optional<Cartao> cartao = cartaoService.buscarPorId(pagamento.getCartaoId());
        if(!cartao.isPresent()){
            return false;
        }
        return true;
    }

    public void deletarPagamento(Pagamento pagamento){
        pagamentoRepository.delete(pagamento);
    }

    public Iterable<Pagamento> buscarTodosPagamentos(List<Integer> pagamentosId){
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAllById(pagamentosId);
        return pagamentos;
    }

    public Iterable<Pagamento> buscarTodosPagamentos() {
        Iterable<Pagamento> pagamentos = pagamentoRepository.findAll();
        return pagamentos;
    }

    public Iterable<Pagamento> buscarFatura(Integer id) {
        Iterable<Pagamento> fatura = pagamentoRepository.findAllByCartaoId(id);
        return fatura;
    }
}
