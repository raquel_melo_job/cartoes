package com.itau.cartoes.services;

import com.itau.cartoes.models.Cliente;
import com.itau.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.*;
import java.util.List;
import java.util.Optional;

@Service
public class ClienteService {

    @Autowired
    private ClienteRepository clienteRepository;

    public Optional<Cliente> buscarPorId(int id){
        Optional<Cliente> optionalCliente = clienteRepository.findById(id);
        return optionalCliente;
    }

    public Cliente salvarCliente(Cliente cliente) {
        Cliente clienteObjeto = clienteRepository.save(cliente);
        return clienteObjeto;
    }

    public void deletarCliente(Cliente cliente){
        clienteRepository.delete(cliente);
    }

}
