package com.itau.cartoes.services;

import com.itau.cartoes.models.Cartao;
import com.itau.cartoes.models.Cliente;
import com.itau.cartoes.models.Pagamento;
import com.itau.cartoes.repositories.CartaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import javax.persistence.*;
import java.util.*;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteService clienteService;

    public Optional<Cartao> buscarPorId(int id){
        Optional<Cartao> optionalCartao = cartaoRepository.findById(id);
        return optionalCartao;
    }

    public Cartao salvarCartao(Cartao cartao) {
        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }

    public boolean clienteExiste(Cartao cartao){
        Optional<Cliente> cliente = clienteService.buscarPorId(cartao.getClienteId());
        if(!cliente.isPresent()){
            return false;
        }
        return true;
    }
    public void deletarCartao(Cartao cartao){
        cartaoRepository.delete(cartao);
    }

    public Iterable<Cartao> buscarTodosCartaos(List<Integer> cartaosId){
        Iterable<Cartao> cartaos = cartaoRepository.findAllById(cartaosId);
        return cartaos;
    }

    public Iterable<Cartao> buscarTodosCartaos() {
        Iterable<Cartao> cartaos = cartaoRepository.findAll();
        return cartaos;
    }

    public Cartao atualizaCartao(Cartao cartao){
        Optional<Cartao> cartaoOptional = buscarPorId(cartao.getId());
        if(cartaoOptional.isPresent()){
            Cartao cartaoData = cartaoOptional.get();
            cartao.setNumero(cartaoData.getNumero());
            cartao.setClienteId(cartaoData.getClienteId());
        }
        Cartao cartaoObjeto = cartaoRepository.save(cartao);
        return cartaoObjeto;
    }
}
