package com.itau.cartoes.repositories;

import com.itau.cartoes.models.Cartao;
import com.itau.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
}
