package com.itau.cartoes.repositories;

import com.itau.cartoes.models.Pagamento;
import org.springframework.data.repository.CrudRepository;

public interface PagamentoRepository extends CrudRepository<Pagamento, Integer> {
    Iterable<Pagamento> findAllByCartaoId(Integer cartaoId);
}
