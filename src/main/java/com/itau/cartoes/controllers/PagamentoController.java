package com.itau.cartoes.controllers;

import com.itau.cartoes.models.Pagamento;
import com.itau.cartoes.services.CartaoService;
import com.itau.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.Optional;

@RestController
@RequestMapping("/pagamento")
public class PagamentoController {
    
    @Autowired
    private PagamentoService pagamentoService;

    @Autowired
    private CartaoService cartaoService;

    @PostMapping()
    public ResponseEntity<Pagamento> adicionarPagamento(@RequestBody Pagamento pagamento){
        if(!pagamentoService.cartaoExiste(pagamento)){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        Pagamento novoPagamento = pagamentoService.salvarPagamento(pagamento);
        return ResponseEntity.status(201).body(novoPagamento);
    }

    @GetMapping("/{id}")
    public Iterable<Pagamento> getFaturaCartao(@PathVariable Integer id){
        Iterable<Pagamento> fatura = pagamentoService.buscarFatura(id);
        return fatura;
    }

    @DeleteMapping("/{id}")
    public Pagamento deletarPagamento(@PathVariable int id){
        Optional<Pagamento> pagamentoOptional = pagamentoService.buscarPorId(id);
        if (pagamentoOptional.isPresent()){
            pagamentoService.deletarPagamento(pagamentoOptional.get());
            return pagamentoOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);

    }
}
