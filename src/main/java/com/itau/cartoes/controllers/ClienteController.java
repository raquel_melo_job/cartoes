package com.itau.cartoes.controllers;

import com.itau.cartoes.models.Cliente;
import com.itau.cartoes.services.ClienteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.*;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
    
    @Autowired
    private ClienteService clienteService;

    @GetMapping("/{id}")
    public Cliente buscarCliente(@PathVariable int id){
        Optional<Cliente> clienteOptional = clienteService.buscarPorId(id);
        if (clienteOptional.isPresent()){
            return clienteOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PostMapping()
    public ResponseEntity<Cliente> adicionarCliente(@RequestBody Cliente cliente){
        Cliente novoCliente = clienteService.salvarCliente(cliente);
        return ResponseEntity.status(201).body(novoCliente);
    }

    @DeleteMapping("/{id}")
    public Cliente deletarCliente(@PathVariable int id){
        Optional<Cliente> clienteOptional = clienteService.buscarPorId(id);
        if (clienteOptional.isPresent()){
            clienteService.deletarCliente(clienteOptional.get());
            return clienteOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);

    }
}
