package com.itau.cartoes.controllers;

import com.itau.cartoes.models.Cartao;
import com.itau.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.persistence.*;
import java.util.Optional;

@RestController
@RequestMapping("/cartao")
public class CartaoController {
    
    @Autowired
    private CartaoService cartaoService;

    @GetMapping("/{id}")
    public Cartao buscarCartao(@PathVariable int id){
        Optional<Cartao> cartaoOptional = cartaoService.buscarPorId(id);
        if (cartaoOptional.isPresent()){
            return cartaoOptional.get();
        }else{
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
    }

    @PatchMapping("/{id}")
    public Cartao atualizaCartao(@PathVariable Integer id, @RequestBody Cartao cartao){
        cartao.setId(id);
        Cartao cartaoObjeto = cartaoService.atualizaCartao(cartao);
        return cartaoObjeto;
    }
    
    @PostMapping()
    public ResponseEntity<Cartao> adicionarCartao(@RequestBody Cartao cartao){
        if(!cartaoService.clienteExiste(cartao)){
            throw new ResponseStatusException(HttpStatus.NO_CONTENT);
        }
        Cartao novoCartao = cartaoService.salvarCartao(cartao);
        return ResponseEntity.status(201).body(novoCartao);
    }

    @DeleteMapping("/{id}")
    public Cartao deletarCartao(@PathVariable int id){
        Optional<Cartao> cartaoOptional = cartaoService.buscarPorId(id);
        if (cartaoOptional.isPresent()){
            cartaoService.deletarCartao(cartaoOptional.get());
            return cartaoOptional.get();
        }
        throw new ResponseStatusException(HttpStatus.NO_CONTENT);

    }
}
